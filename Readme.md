# WFFLIX

## Samenvatting:
Dit project is onderdeel van de module Database, Ontwerpen, Modelleren en Programmeren van Windesheim in Almere. Het is de bedoeling om een website te bouwen voor WFFLIX, aan de hand van HTML en PHP, om dit te beheersen.

In de root van ons project vinden we nu een aantal bestanden. de <kbd>index.php</kbd>, de <kbd>.gitignore</kbd> en de <kbd>Readme.md</kbd>.

## How to run dev
1: Laadt SQL in de database
2: Run command (Composer dump-autoload)
3: Start localhost met de command (php -S localhost:8080)

$dbName = 'WFFlix';
$hostName = 'localhost';
$userName = 'root';
$password = 'root';

## How to deploy
1: Login SSH in B3
2: Run het command (cd var/www/html/wfflix)
3: Run het command (git pull)

$dbName = 'WFFlix';
$hostName = 'b3.clow.nl';
$userName = 'wfflix_user';
$password = 'sKy35yOpnmDnVVBZY6Liahtb0rxdo0h9';

## Gebruikers acounts
Dummy accounts die worden aangemaakt bij het initializeren van de database:

Admins:

    E-mail: r.borgstede@windesheim.nl
    Password: admin
    
    E-mail: s.hoeksema@windesheim.nl
    Password: admin

    E-mail: admin@email.nl
    Password: admin  


Creator:

    E-mail: creator@email.nl 
    Password: creator  


Users:

    E-mail: user@email.nl 
    Password: user

      
Voor video's uploaden bij het admin panel is er een limiet aan de bestand grootte. Deze wordt geregeld door PHP zelf dus voor je eigen testomgeving moet je zelf deze max grootte aanpassen.
php.ini file size limiters:
    upload_max_filesize = *
    post_max_size = *
