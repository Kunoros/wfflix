CREATE OR REPLACE DATABASE WFFlix;

USE WFFlix;

CREATE OR REPLACE TABLE user
(
    id           INT PRIMARY KEY                      NOT NULL AUTO_INCREMENT,
    firstName    VARCHAR(64)                          NOT NULL,
    lastName     VARCHAR(64)                          NOT NULL,
    userName     VARCHAR(64)                          NOT NULL,
    email        VARCHAR(64)                          NOT NULL,
    password     VARCHAR(64)                          NOT NULL,
    subscription TINYINT(1)                           NOT NULL DEFAULT 0,
    role         ENUM ('STUDENT', 'CREATOR', 'ADMIN') NOT NULL DEFAULT 'STUDENT',
    roleapproval TINYINT(1)                                    DEFAULT 0,
    createdAt    DATETIME                                      DEFAULT NOW(),
    updatedAt    DATETIME                                      DEFAULT NOW()
);

-- password: test, username: testUser
INSERT INTO user (id, firstName, lastName, userName, email, password, subscription)
VALUES (1, 'test', 'last', 'testUser', 'test@email.com', '$2y$10$G2muv31lHJEziNulyL7jN.IQW1MwBSSYwxXS8X0Ixp3nKiZ9xvzBK',
        1);

-- password: admin, username: admin
INSERT INTO user (id, firstName, lastName, userName, email, password, subscription, role)
VALUES (4, 'admin', 'admin', 'admin', 'admin@email.com', '$2y$10$6PJ.9F8rGtnZJs2FBrG9huaWXxyjEP7BYrls53lWaVVTxLjTNE1AS',
        2, 'ADMIN');

-- password: creator
INSERT INTO user (id, firstName, lastName, userName, email, password, subscription, role)
VALUES (5, 'Creator', 'test', 'creator', 'creator@email.com', '$2y$10$y9Byc7ghjAstSaH2eIFVWu2aXlFdrXvmneZEJjrCirp9Nu4PoO0mK',
        2, 'CREATOR');

CREATE OR REPLACE TABLE course
(
    id                INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    courseName        VARCHAR(64)     NOT NULL,
    courseDescription VARCHAR(500)    NOT NULL,
    thumbnail         LONGBLOB        NOT NULL,
    thumbnailType     VARCHAR(64),
    video             LONGBLOB        NOT NULL,
    videoType         VARCHAR(64),
    rating            FLOAT,
    userId            INT             NOT NULL,
    createdAt         DATETIME DEFAULT NOW(),
    updatedAt         DATETIME DEFAULT NOW(),
    FOREIGN KEY (userId) REFERENCES user (id) ON DELETE CASCADE
);
-- [2021-10-04 15:09:35] [HY000][1005] (conn=5) Can't create table `wfflix`.`documents` (errno: 150 "Foreign key constraint is incorrectly formed")
-- FIX: changed FOREIGN KEY (documentId) REFERENCES course (document) into FOREIGN KEY (documentId) REFERENCES course (id)

CREATE OR REPLACE TABLE documents
(
    id        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    courseId  INT             NOT NULL,
    name      VARCHAR(256)    NOT NULL,
    data      LONGBLOB        NOT NULL,
    dataType  VARCHAR(64)     NOT NULL,
    createdAt DATETIME DEFAULT NOW(),
    updatedAt DATETIME DEFAULT NOW(),
    FOREIGN KEY (courseId) REFERENCES course (id) ON DELETE CASCADE
);

CREATE OR REPLACE TABLE comment
(
    id        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    comment   VARCHAR(500)    NOT NULL,
    userId    INT             NOT NULL,
    courseId  INT             NOT NULL,
    createdAt DATETIME DEFAULT NOW(),
    updatedAt DATETIME DEFAULT NOW(),
    FOREIGN KEY (userId) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (courseId) REFERENCES course (id) ON DELETE CASCADE
);

CREATE OR REPLACE TABLE review
(
    id        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    reviewId  INT             NOT NULL,
    courseId  INT             NOT NULL,
    userId    INT             NOT NULL,
    message   VARCHAR(500)    NOT NULL,
    createdAt DATETIME DEFAULT NOW(),
    updatedAt DATETIME DEFAULT NOW(),
    FOREIGN KEY (courseId) REFERENCES course (id),
    FOREIGN KEY (userId) REFERENCES user (id)
)