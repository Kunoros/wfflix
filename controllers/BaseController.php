<?php
class BaseController
{
    protected function RenderView(string $view, $data = [])
    {

        extract($data, EXTR_OVERWRITE);
        require './views/_partials/header.php';
        require './views/_partials/navbar.php';
        require 'views/' . $view . '.php';
        //require './views/_partials/footer.php';
    }
}