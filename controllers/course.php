<?php
class specificCourse extends BaseController
{
    public $result = [];

    public function getCourseView()
    {
        $course = Course::showSpecificCourse($_GET['id']);
        $this->RenderView('course', ['course' => $course]);
    }

    public function postComment() {
        $courseId = (int) substr($_SERVER["HTTP_REFERER"], -1);

        if (isset($_SESSION['user']) && isset($_POST['submit'])) {
            Comment::postComment($_POST['comment'], $_SESSION['user']->getId(), $courseId);
            header('Location: /course?page=course&id=' . $courseId);
        } else {
            header('Location: /course?page=course&id=' . $courseId);
        }
    }

    public function getVideo()
    {
        $video = Course::getVideoBlob($_GET['id']);
        if ($video) {
            header("Content-Type: " . $video['contentType']);
            echo ($video['lob']);
        } else {
            header("Content-Type: video/mp4");
            readfile('assets/img/no-img-found.png');
        }
    }
}
