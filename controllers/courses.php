<?php

class courses extends BaseController
{
    public $result = [];

    public function getCoursesView()
    {
        $allCourses = Course::getAllCourses();
        $this->RenderView('courses', ["allCourses" => $allCourses]);
    }

    public function getImage()
    {
        $image = Course::getImageBlob($_GET['id']);
        if ($image) {
            header("Content-Type: " . $image['contentType']);
            echo ($image['lob']);
        } else {
            header("Content-Type: image/png");
            readfile('assets/img/no-img-found.png');
        }
    }

    public function getSearch()
    {
        $results = Course::search('%' . $_POST["search"] . '%');
        $this->RenderView("courses", ['allCourses' => $results]);
    }

    public function uploadDocument()
    {
        $course = Course::showSpecificCourse($_POST['courseId']);
        Document::createDocument($course, $_FILES['file']['tmp_name'], $_FILES['file']['type'], $_FILES['file']['name']);
        header('Location: /course?page=course&id=' . $course->getCourseId());
    }

    public function getDocument()
    {
        $document = Document::getBlob($_GET['id']);

        if ($document) {
            header('Content-Disposition: attachment; filename="' . $document['contentName'] . '"');
            header("Content-Type: " . $document['contentType']);
            echo ($document['lob']);
        }
        else http_response_code(404);
    }
}