<?php
class home extends BaseController {

    public function getHomeView()
    {
        $allCourses = Course::getRandomCourses();
        $this->RenderView('home', ["allCourses" => $allCourses]);
    }


}