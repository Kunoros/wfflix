<?php
class login extends BaseController {

    public function getLoginView()
    {
        $this->RenderView('login' );
    }

    public function loginUser() {
        $user = User::userLogin($_POST);
        if ($user && isset($_POST['submit'])) {
            $_SESSION['user'] = $user;
            header('Location: /');
        }
    }

    public function logoutUser() {
        //! End user session $ redirect to home page.
        session_destroy();
        header('Location: /');
    }
}