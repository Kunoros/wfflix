<?php

class profile extends BaseController {

    public $course = [];
    public function getProfileView()
    {
        $type = 0;
        $id = 0;

        $url = $_SERVER['REQUEST_URI'];
        $queryStr = parse_url($url, PHP_URL_QUERY);
        parse_str($queryStr, $queryParams);

        extract($queryParams);
        $ur = "\$type = $type  \$id = $id";
        if($type == 'delete'){
            echo $id;
            Course::delete($id,$type);
        }

        $course = Course::getMyCourses();
        $this->RenderView('profile', ['Mycourse' =>$course]);

        // var_dump($_SESSION['user']->getFirstName());
    }


    public function profileUpdate() {
        $user = User::updateUser($_POST);
        if ($user && isset($_POST['submit'])) {
            $_SESSION['user'] = $user;
            header('Location: /profile?msg=Profile updated&type=success');
        }
    }
}