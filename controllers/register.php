<?php
class register extends BaseController
{

    public function getRegisterView() {
        $this->RenderView('register');
    }

    public function register() {
        if (isset($_POST['submit'])) {
            User::createUser($_POST);
        }
    }

}