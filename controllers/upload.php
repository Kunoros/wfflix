<?php

class upload extends BaseController
{

    public function getUploadView()
    {
        $this->RenderView('upload');
    }

    public function VideoUpload()
    {
        if (isset($_POST['submit'])) {
            Course::VideoUpload($_POST);
        }
    }

}