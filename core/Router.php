<?php

class Router
{

    public static function startUp() {
        require 'routes.php';
	$requestUri = $_SERVER['REQUEST_URI'];
        if(Router::isProduction()) {
            $requestUri = str_replace("/wfflix", "", $requestUri);
        }
	$route = trim(explode('?', $requestUri)[0], '/');
        if (array_key_exists($route, $routes[$_SERVER['REQUEST_METHOD']])) {

            //Check if there are roles set on the current router.
            if(isset($routes[$_SERVER['REQUEST_METHOD']][$route]['roles'])) {
                $roles = $routes[$_SERVER['REQUEST_METHOD']][$route]['roles'];

                //Check if roles is not empty
                if(!empty($roles)) {

                    //If there is not: a user AND user role is in roles array. head to /login
                    if(!(isset($_SESSION['user']) && in_array($_SESSION['user']->getRole(), $roles))) {
                        header('Location: /login');
                        return;
                    }
                }
            };

            $getController = new $routes[$_SERVER['REQUEST_METHOD']][$route]['controller']();
            $getController->{$routes[$_SERVER['REQUEST_METHOD']][$route]['method']}(); 
         }
    }

    public static function isProduction()
    {
        return ($_SERVER['SERVER_NAME'] == 'b3.clow.nl');
    }
}


