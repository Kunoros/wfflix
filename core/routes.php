<?php

//require './views/_partials/header.php';
//require './views/_partials/navbar.php';

$routes = [
    'GET' => [
        '' => [
            'controller' => 'home',
            'method' => 'getHomeView'
        ],
        'home' => [
            'controller' => 'home',
            'method' => 'getHomeView',
            'roles' => ['ADMIN', 'CREATOR']
        ],
        'about' => [
            'controller' => 'about',
            'method' => 'getAboutView',
        ],
        'courses' => [
            'controller' => 'courses',
            'method' => 'getCoursesView'
        ],
        'courses/image' => [
            'controller' => 'courses',
            'method' => 'getImage'
        ],
        'course' => [
            'controller' => 'specificCourse',
            'method' => 'getCourseView'
        ],
        'course/video' => [
            'controller' => 'specificCourse',
            'method' => 'getVideo'
        ],
        'contact' => [
            'controller' => 'contact',
            'method' => 'getContactView'
        ],
        'login' => [
            'controller' => 'login',
            'method' => 'getLoginView'
        ],
        'register' => [
            'controller' => 'register',
            'method' => 'getRegisterView',
        ],
        'logout' => [
            'controller' => 'login',
            'method' => 'logoutUser'
        ],
        'profile' => [
            'controller' => 'profile',
            'method' => 'getProfileView',
        ],
        'upload' => [
            'controller' => 'upload',
            'method' => 'getUploadView',
        ],
        'admin' => [
            'controller' => 'admin',
            'method' => 'getAdminView',
        ],
        'courses/documents' => [
            'controller' => 'courses',
            'method' => 'getDocument'
        ]
    ],
    'POST' => [
        'register-post' => [
            'controller' => 'register',
            'method' => 'register'
        ],
        'contact-post' => [
            'controller' => 'contact',
            'method' => 'sendContactEmail'
        ],
        'login-post' => [
            'controller' => 'login',
            'method' => 'loginUser'
        ],
        'admin' => [
            'controller' => 'admin',
            'method' => 'roleApproval',
        ],
        'update-profile' => [
            'controller' => 'profile',
            'method' => 'profileUpdate'
        ],
        'upload-post' => [
            'controller' => 'upload',
            'method' => 'VideoUpload'
        ],
        'tmp-endpoint' => [
            'controller' => 'courses',
            'method' => 'dumpData'
        ],
        'search' => [
            'controller' => 'courses',
            'method' => 'getSearch'
        ],
        'comment-post' => [
            'controller' => 'specificCourse',
            'method' => 'postComment'
        ],
        'courses/documents' => [
            'controller' => 'courses',
            'method' => 'uploadDocument',
            'roles' => ['ADMIN', 'CREATOR']
        ]

    ],
];