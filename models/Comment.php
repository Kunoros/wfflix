<?php 

class Comment extends ConnectDB
{
    private int $commentId;
    private string $message;
    private DateTime $createdAt, $updatedAt;
    private User $user;

    public function __construct(int $commentId, string $message, DateTime $createdAt, DateTime $updatedAt, User $user)
    {
        $this->commentId = $commentId;
        $this->message = $message;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->user = $user;
    }
    
    public static function getComments($id)
    {
        $dbConn = self::connectDB();
        $query = "SELECT *, comment.comment AS comment_message FROM comment
        JOIN user ON user.id = comment.userId
        WHERE comment.courseId = :courseId";

        $stmt = $dbConn->prepare($query);
        $stmt->bindValue(":courseId", $id);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $results;
    }

    public static function postComment($comment, $userId, $courseId)
    {
        $dbConn = self::connectDB();
        $stmt = $dbConn->prepare("INSERT INTO comment (comment, userId, courseId) VALUES (:comment, :userId, :courseId)");

        $stmt->bindValue(":comment", $comment);
        $stmt->bindValue(":userId", $userId);
        $stmt->bindValue("courseId", $courseId);
        $stmt->execute();
        
        return;
    }

    /**
     * Get the value of commentId
     */ 
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * Get the value of comment
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get the value of updatedAt
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }
}