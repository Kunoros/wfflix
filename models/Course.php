<?php

class Course extends ConnectDB
{
    private int $courseId;
    private string $courseName;
    private string $courseDescription;
    private float $rating;
    private DateTime $createdAt, $updatedAt;
    private User $user;
    private array $comments;

    public function __construct(int $courseId, string $courseName, string $courseDescription, float $rating, DateTime $createdAt, DateTime $updatedAt, User $user, array $comments)
    {
        $this->courseId = $courseId;
        $this->courseName = $courseName;
        $this->courseDescription = $courseDescription;
        $this->rating = $rating;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->user = $user;
        $this->comments = $comments;
    }

    public static function getAllCourses()
    {
        $dbConn = self::connectDB();
        $query = "SELECT course.id, courseName, courseDescription, rating, course.userId, firstName, lastName, userName, email, subscription, role, course.createdAt, course.updatedAt FROM course
                JOIN user ON course.userId = user.id";

        $stm = $dbConn->prepare($query);
        $stm->execute();
        $allCourses = $stm->fetchAll(PDO::FETCH_ASSOC);
        $returnArray = [];
        foreach ($allCourses as $course) {
            $individualCourse = new Course(
                $course['id'],
                $course['courseName'],
                $course['courseDescription'],
                $course['rating'],
                new DateTime($course['createdAt']),
                new DateTime($course['updatedAt']),
                new User(
                    $course['userId'],
                    $course['firstName'],
                    $course['lastName'],
                    $course['userName'],
                    $course['email'],
                    $course['role'],
                    $course['subscription'],
                ),
                []
            );
            $returnArray[] = $individualCourse;
        }
        return $returnArray;
    }

    public static function showSpecificCourse($id)
    {
        $dbConn = self::connectDB();
        $query = "SELECT course.id, courseName, courseDescription, rating, course.userId, firstName, lastName, userName, email, subscription, role, course.createdAt, course.updatedAt FROM course
        JOIN user ON course.userId = user.id
        WHERE course.id = :id";
        $stmt = $dbConn->prepare($query);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $course = $stmt->fetch(PDO::FETCH_ASSOC);

        $comments = Comment::getComments($id);
        $commentArray = [];

        foreach($comments as $comment) {
            $commentArray[] = new Comment(
                $comment['id'],
                $comment['comment_message'],
                new DateTime($comment['createdAt']),
                new DateTime($comment['updatedAt']),
                new User(
                    $comment['userId'],
                    $comment['firstName'],
                    $comment['lastName'],
                    $comment['userName'],
                    $comment['email'],
                    $comment['subscription'],
                    $comment['role'],
                )
            );
        }

        $specificCourse = new Course(
            $course['id'],
            $course['courseName'],
            $course['courseDescription'],
            $course['rating'],
            new DateTime($course['createdAt']),
            new DateTime($course['updatedAt']),
            new User(
                $course['userId'],
                $course['firstName'],
                $course['lastName'],
                $course['userName'],
                $course['email'],
                $course['role'],
                $course['subscription'],
            ),
            $commentArray
        );
        return $specificCourse;
    }

    public function getDocumentList() {
        $dbConn = self::connectDB();
        $query = "SELECT * FROM documents JOIN course ON documents.courseId = course.id WHERE courseId = :id";
        $stmt = $dbConn->prepare($query);
        $stmt->execute(['id'=> $this->courseId]);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $returnArray = [];
        foreach($results as $document) {
            $returnArray[] = new Document(
                $document['id'],
                $document['name'],
                $document['dataType'],
                new DateTime($document['createdAt']),
                new DateTime($document['updatedAt'])
            );
        }
        return $returnArray;
    }

    public static function getImageBlob($id)
    {
        $db = self::connectDB();
        $stmt = $db->prepare('SELECT thumbnailType, thumbnail FROM course WHERE id = :id');

        $stmt->execute(['id' => $id]);

        $stmt->bindColumn('thumbnailType', $type, PDO::PARAM_STR, 256);
        $stmt->bindColumn('thumbnail', $lob, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);

        if ($type == null || $lob == null) {
            return null;
        } else {
            return [
                'contentType' => $type,
                'lob' => $lob
            ];
        }
    }

    public static function getVideoBlob($id)
    {
        $db = self::connectDB();
        $stmt = $db->prepare('SELECT videoType, video FROM course WHERE id = :id');

        $stmt->execute(['id' => $id]);

        $stmt->bindColumn('videoType', $type, PDO::PARAM_STR, 256);
        $stmt->bindColumn('video', $lob, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);

        if ($type == null || $lob == null) {
            return null;
        } else {
            return [
                'contentType' => $type,
                'lob' => $lob
            ];
        }
    }

    public static function search(string $search)
    {
        $dbConn = self::connectDB();
        $query = "SELECT course.id AS course_id, courseName, courseDescription, rating, course.userId, firstName, lastName, userName, email, subscription, role, course.createdAt, course.updatedAt 
                    FROM course
                    JOIN user ON course.userId = user.id
                    WHERE courseName LIKE :search";

        $smt = $dbConn->prepare($query);
        $smt->bindparam(":search", $search);
        $smt->execute();
        $results = $smt->fetchAll(PDO::FETCH_ASSOC);

        $courseList = [];
        foreach ($results as $result) {
            $course = new Course(
                $result['course_id'],
                $result['courseName'],
                $result['courseDescription'],
                $result['rating'],
                new DateTime($result['createdAt']),
                new DateTime($result['updatedAt']),
                new User(
                    $result['userId'],
                    $result['firstName'],
                    $result['lastName'],
                    $result['userName'],
                    $result['email'],
                    $result['role'],
                    $result['subscription'],
                ),
                []
            );
            $courseList[] = $course;
        }
        return $courseList;
    }

    public static function videoUpload($data)
    {
        if ($_SESSION['user']->getRole() == 'CREATOR') {
            $dbConn = self::connectDB();
            $stmt = $dbConn->prepare("insert into course (rating,courseName,courseDescription,thumbnail,thumbnailType, video,videoType,userId) values (:rating,:courseName,:courseDescription,:imagedata,:imageType,:videodata,:videoType,:id)");

            $image = fopen($_FILES['file']['tmp_name'], 'rb');
            $video = fopen($_FILES['video']['tmp_name'], 'rb');
            $id = $_SESSION['user']->getId();
            $rating = 0.0;
            $stmt->bindParam('rating', $rating);
            $stmt->bindParam('courseName', $data['courseName']);
            $stmt->bindParam('courseDescription', $data['courseDescription']);
            $stmt->bindParam('imagedata', $image, PDO::PARAM_LOB);
            $stmt->bindParam('imageType', $_FILES['file']['type']);
            $stmt->bindValue("videodata", $video, PDO::PARAM_LOB);
            $stmt->bindValue("videoType", $_FILES['video']['type']);
            $stmt->bindParam('id', $id);
            $stmt->execute();
            header('Location: /upload?upload=succes inserted into data base');
        }
        else{header('Location: /upload?upload=now rights');}
    }

    public static function getMyCourses()
    {
        $dbConn = self::connectDB();
        $query = "SELECT course.id as course_id,courseName,courseDescription,rating, user.* FROM course JOIN user ON course.userId = user.id WHERE userId = :id";
        $user = $_SESSION['user']->getId();
        $smt = $dbConn->prepare($query);
        $smt->bindparam('id' , $user);
        $smt->execute();
        $MyCourses = $smt->fetchAll(PDO::FETCH_ASSOC);



        $MyCourseList = [];
        foreach ($MyCourses as $Mycourse) {
            $course = new Course(
                intval($Mycourse['course_id']),
                $Mycourse['courseName'],
                $Mycourse['courseDescription'],
                floatval($Mycourse['rating']),
                new DateTime($Mycourse['createdAt']),
                new DateTime($Mycourse['updatedAt']),
                new User(
                    $Mycourse['id'],
                    $Mycourse['firstName'],
                    $Mycourse['lastName'],
                    $Mycourse['userName'],
                    $Mycourse['email'],
                    $Mycourse['role'],
                    $Mycourse['subscription'],
                ),
                []

            );
            $MyCourseList[] = $course;
        }

        return $MyCourseList;


    }

    public static function delete($id,$type)
    {
        $dbConn = self::connectDB();
        if($_SESSION['user']->getRole() == 'CREATOR')
            if($type == 'delete'){

                $query = "DELETE 
                    FROM course WHERE id = :id";
                $stm = $dbConn->prepare($query);
                $stm->execute(['id' => $id]);
                header('Location: /profile');

            }
    }

    public static function getRandomCourses()
    {
        $dbConn = self::connectDB();
        $query = "SELECT course.id, courseName, courseDescription, rating, course.userId, firstName, lastName, userName, email, subscription, role, course.createdAt, course.updatedAt 
                FROM course
                JOIN user ON course.userId = user.id
                order by RAND() limit 3";

        $stm = $dbConn->prepare($query);
        $stm->execute();
        $allCourses = $stm->fetchAll(PDO::FETCH_ASSOC);

        foreach ($allCourses as $course) {
            $individualCourse = new Course(
                $course['id'],
                $course['courseName'],
                $course['courseDescription'],
                $course['rating'],
                new DateTime($course['createdAt']),
                new DateTime($course['updatedAt']),
                new User(
                    $course['userId'],
                    $course['firstName'],
                    $course['lastName'],
                    $course['userName'],
                    $course['email'],
                    $course['role'],
                    $course['subscription'],
                ),
                []
            );
            $returnArray[] = $individualCourse;
        }
        return $returnArray;
    }

    /**
     * Get the value of id
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Get the value of courseName
     */
    public function getCourseName()
    {
        return $this->courseName;
    }

    /**
     * Get the value of courseDescription
     */
    public function getCourseDescription()
    {
        return $this->courseDescription;
    }

    /**
     * Get the value of thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Get the value of video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Get the value of rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Get the value of userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get the value of user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get the value of updatedAt
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get the value of comments
     */ 
    public function getComments()
    {
        return $this->comments;
    }
}
