<?php

class Document extends ConnectDB
{

    private int $id;
    private string $name, $dataType;
    private DateTime $createdAt, $updatedAt;

    /**
     * Document constructor.
     * @param int $id
     * @param string $name
     * @param string $dataType
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     */
    public function __construct(int $id, string $name, string $dataType, DateTime $createdAt, DateTime $updatedAt)
    {
        $this->id = $id;
        $this->name = $name;
        $this->dataType = $dataType;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }


    public static function getBlob($documentId)
    {
        $db = self::connectDB();
        $stmt = $db->prepare('SELECT data, dataType, name FROM documents WHERE id = :id');
        $stmt->execute(['id' => $documentId]);
        $stmt->bindColumn('dataType', $type, PDO::PARAM_STR, 256);
        $stmt->bindColumn('name', $name, PDO::PARAM_STR, 256);
        $stmt->bindColumn('data', $lob, PDO::PARAM_LOB);
        $stmt->fetch(PDO::FETCH_BOUND);
        if ($type == null || $lob == null) {
            return null;
        } else {
            return [
                'contentName' => $name,
                'contentType' => $type,
                'lob' => $lob
            ];
        }
    }

    public static function createDocument(Course $course, string $tmpName, string $contentType, string $name)
    {
        $db = self::connectDB();
        $stmt = $db->prepare("INSERT INTO documents (courseId, data, dataType, name) values (:courseId, :data, :dataType, :name)");

        $fp = fopen($tmpName, 'r');
        $courseId = $course->getCourseId();

        $stmt->bindParam(':data', $fp, PDO::PARAM_LOB);
        $stmt->bindParam(':dataType', $contentType);
        $stmt->bindParam(':courseId', $courseId);
        $stmt->bindParam(':name', $name);
        $db->beginTransaction();
        $stmt->execute();
        $db->commit();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }
}