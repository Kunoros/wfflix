<?php

class User extends ConnectDB
{

    private int $id;
    private string $firstName;
    private string $lastName;
    private string $userName;
    private string $email;
    private string $role;
    private bool $subscription;

    /**
     * User constructor.
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $userName
     * @param string $email
     * @param string $role
     * @param bool $subscription
     * @param string $password
     */

    public function __construct(int $id, string $firstName, string $lastName, string $userName, string $email, string $role, bool $subscription)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->userName = $userName;
        $this->email = $email;
        $this->role = $role;
        $this->subscription = $subscription;
    }

    public static function checkUser($email)
    {
        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("SELECT email FROM user WHERE email = :email");

        $stm->bindValue(':email', $email);
        $stm->execute();

        $emailExist = $stm->fetch();
        if ($emailExist) {
            return false;
        } else {
            return true;
        }
    }

    public static function createUser($data)
    {

        $dbConn = self::connectDB();
        $password = password_hash($data['password'], PASSWORD_BCRYPT);
        $stm = $dbConn->prepare("INSERT INTO user (firstName, lastName, userName, email, password, subscription) VALUES (:firstName, :lastName, :userName, :email, :password, :subscription)");

        $stm->bindParam(':firstName', $data['firstName']);
        $stm->bindParam(':lastName', $data['lastName']);
        $stm->bindParam(':userName', $data['userName']);
        $stm->bindParam(':email', $data['email']);
        $stm->bindParam(':password', $password);
        //! Got a error that the type of false & true cant be used so use tinyIn
        $stm->bindValue(':subscription', 0);

        if (User::checkUser($data['email']) === false) {
            header('Location:/register?register=email already in use');
        } else {
            $stm->execute();
            header('Location: /login');
        }
    }

    public static function userLogin($data)
    {
        $userEmail = $data['email'];
        $userPassword = $data['password'];

        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("SELECT * FROM user WHERE email = :email");
        $stm->bindParam(':email', $userEmail);

        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch(PDO::FETCH_ASSOC);
            if (password_verify($userPassword, $user['password'])) {
                return new User($user['id'], $user['firstName'], $user['lastName'], $user['userName'], $user['email'], $user['role'], $user['subscription'], $user['password']);
            } else {
                header('Location: /login?login=wrong password');
            }
        } else {
            header('Location: /login?login=email not found');
        }
    }

    public static function updateUser($data)
    {
        $user = User::getUser($data['id']);

        if ($user) {
            $passwordChange = false;
            if ($data['old_password'] !== '' && $data['new_password'] !== '' && $data['new_password2'] !== '') {
                if (User::verifyUserPassword($data['id'], $data['old_password']) && $data['new_password'] == $data['new_password2']) {
                    $passwordChange = true;
                }
            }

            $dbConn = self::connectDb();
            if ($data['email'] == $user->email) {
                $stm = $dbConn->prepare("UPDATE user SET firstName = :firstName, lastName = :lastName, userName = :userName" . ($passwordChange ? ', password = :password' : '') . " WHERE id= :id");
            } else {
                $stm = $dbConn->prepare("UPDATE user SET email = :email, firstName = :firstName, lastName = :lastName, userName = :userName" . ($passwordChange ? ', password = :password' : '') . " WHERE id= :id");
                $stm->bindValue(':email', $data['email']);
            }

            $stm->bindValue(':firstName', $data['firstName']);
            $stm->bindValue(':lastName', $data['lastName']);
            $stm->bindValue(':userName', $data['userName']);
            $stm->bindValue(':id', $user->id);

            if ($passwordChange) $stm->bindValue(':password', password_hash($data['new_password'], PASSWORD_BCRYPT));
            if ($stm->execute()) {
                return User::getUser($user->id);
            } else {
                header('Location: /profile?edit=Something went wrong try again!');
                exit;
            }
        } else {
            header('Location: /profile?edit=User not found!');
            exit;
        }
    }

    public static function getUser($id)
    {
        $dbConn = self::connectDB();
        $stm = $dbConn->prepare("select * from user where id = :id");
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch();
            return new User(
                $user['id'],
                $user['firstName'],
                $user['lastName'],
                $user['userName'],
                $user['email'],
                $user['role'],
                $user['subscription'],
            );
        }
        return null;
    }

    public static function verifyUserPassword($id, $password)
    {
        $dbConn = self::connectDb();
        $stm = $dbConn->prepare("SELECT * FROM user WHERE id = :id");
        $stm->bindParam(':id', $id);
        if ($stm->execute() && $stm->rowCount() > 0) {
            $user = $stm->fetch();
            if (password_verify($password, $user['password'])) {
                return true;
            } else {
                header('Location: /profile?edit=Verify went wrong!');
                exit;
            }
        }
    }

    public static function getAll()
    {
        $dbConn = self::connectDB();
        $query = "
            SELECT * FROM user
        ";
        $stm = $dbConn->prepare($query);
        $stm->execute();
        $results = $stm->fetchAll(PDO::FETCH_ASSOC);
        $returnarray = [];
        foreach ($results as $user) {
            $newUser = new User(
                $user['id'],
                $user['firstName'],
                $user['lastName'],
                $user['userName'],
                $user['email'],
                $user['role'],
                $user['subscription'],
                $user['roleapproval']
            );
            $returnarray[] = $newUser;
        }
        return $returnarray;
    }

    public static function getStudentsAndCreators()
    {
        $dbConn = self::connectDB();
        $query = "SELECT * FROM user WHERE role IN ('STUDENT', 'CREATOR')";
        $stm = $dbConn->prepare($query);
        $stm->execute();
        return $stm->fetchAll();
    }

    public static function validateRole(int $userId, string $role)
    {
        $dbConn = self::connectDB();
        $query = 'UPDATE user SET role = :role, roleapproval = true, updatedAt = NOW() WHERE id = :userId';
        $creatorRole = 'CREATOR';
        $studentRole = 'STUDENT';

        $stm = $dbConn->prepare($query);
        $stm->bindParam(':userId', $userId);
        $stm->bindParam(':role', $role);
        $stm->execute();
        $results = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getSubscription()
    {
        return $this->subscription;
    }
}