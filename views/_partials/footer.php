<link rel="stylesheet" href="assets/css/footer.css">

<!-- Footer -->
<footer class="text-white text-center">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-0"></h5>
                <div class="col-sm-4 col-md-4">
                    <div class="widget-title">
                        <a class="navbar-brand" href=""><img class="logo-resize" alt="wifflix-logo" src="assets/img/wfflix-logo-1.png"></a>
                    </div>
                </div>
            </div>
            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">We are WFFLIX and we are here for you</h5>
            </div>
            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">Start learning</h5>
                <ul class="list-unstyled mb-0">
                    <li>
                        <a class="nav-link" href="courses">Courses</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">More info</h5>
                <ul class="list-unstyled">
                    <li>
                        <a class="nav-link" href="about">About</a>
                    </li>
                    <li>
                        <a class="nav-link" href="contact">Contact Us</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
        <!-- Copyright -->
        <div class="text-center p-3">
            <span>© 2021 Copyright:</span>
            <a class="footer-black-a" href=".">WFFLIX.nl</a>
        </div>
        <!-- Copyright -->
    </footer>
<!-- Footer -->

<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
