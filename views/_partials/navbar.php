<link rel="stylesheet" href="assets/css/navbar.css">

<nav class="navbar navbar-expand-lg navbar-light navBackground">
    <div class="container-fluid">
        <a class="navbar-brand" href="."><img class="logo-resize" alt="apple-img" src="./assets/img/wfflix-logo-1.png"></a>
        <button class="navbar-toggler search-btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse flex-fix log-in-out-mb" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link tab-font-size text-white navbar-tab" href="courses">Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link tab-font-size text-white navbar-tab" href="about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link tab-font-size text-white navbar-tab" href="contact">Contact</a>
                </li>
                <?php if(isset($_SESSION['user'])) : if($_SESSION['user']->getRole() == 'ADMIN') : ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link tab-font-size text-white navbar-tab dropdown-toggle" href="#" id="adminDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Administratie</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="admin">Goedkeuring</a></li>
                        </ul>
                    </li>
                <?php endif; endif;?>
                <?php if(isset($_SESSION['user'])) : if($_SESSION['user']->getRole() == 'CREATOR') : ?>
                    <li class="nav-item">
                        <a class="nav-link tab-font-size text-white navbar-tab" href="upload">Upload course</a>
                    </li>
                <?php endif; endif;?>
            </ul>
            <form action="search" method="POST" class="d-flex w-50 extra-form-pd">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="search">
            </form>
            <?php if (isset($_SESSION['user'])) { ?>
                <div class="log-in-out-mb">
                    <a class="btn btn-outline-light" href="profile"> Profile</a>
                    <a class="btn btn-outline-light" href="logout"> Logout</a>
                </div>
            <?php } else { ?>
                <div class="log-in-out-mb">
                    <a class="btn btn-outline-light" href="login"> Log in</a>
                    <a class="btn btn-outline-light" href="register"> Sign up</a>
                </div>
            <?php } ?>
        </div>
    </div>
</nav>