<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <link rel="stylesheet" href="assets/css/about.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Required bootstrap from _partials -->


    <title>About</title>
</head>
<body>
<div class="container extra-margin main-container">
        <!--slogan-->
        <div class="row slogan-text">
            <div class="col"></div>
            <div class="col-12">
                <h1 class="slogan">WE ARE WFFLIX AND WE ARE HERE FOR YOU</h1>
            </div>
        </div>
        <!--slogan-->
                <!--images-->
    <div class="row">
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/c-sharp-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/python-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/java-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/swift-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/go-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <div class="col-lg-2 col-md-6" class="responsive">
            <img src="assets/img/php-course.png" class="image-resize-one img-fluid" width=300" height="300">
        </div>
        <!--images-->
        <!--title-->
        <div class="row">
            <div class="col"></div>
            <div class="col-12">
                <h2 class="h2-text">About WFFLIX</h2>
            </div>
            <div class="col"></div>
        </div>
        <!--title-->
        <!--paragraph 1-->
        <div class="row">
            <div class="col"></div>
            <div class="col-9">Our mission is to offer a user friendly platform with a wide range of programming
                courses that are quick to learn, or to repeat. To max out the skill you have been imagining in no time!
            </div>
            <div class="col"></div>
        </div>
        <!--paragraph 1-->
    </div>
</div>

<?php require '_partials/footer.php'; ?>
</body>
</html>
