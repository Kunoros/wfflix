<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/register.css">
    <link rel="stylesheet" href="assets/css/admin.css">
    <title>Admin</title>
</head>

<body>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="container col-md-8 mb-4 pt-4">
            <?php foreach ($users as $user) : ?>
                <div class="row mb-4 pt-2 borders admin-form">
                    <div class="col-md-4" style="border-right: 1px solid">
                        <div class="row text-white">
                            <div class="col-md-12">
                                <strong>User gegevens :</strong> <br>
                                <?= $user['firstName'] ?>&nbsp;<?= $user['lastName'] ?> <br>
                                <?= $user['email'] ?> <br>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="align-text-left text-white">
                            <strong>Roles :</strong>
                            <form action="" method="post">
                                <input type="hidden" name="userId" value="<?= $user['id'] ?>">

                                <select class="form-select" name="role" aria-label="Default select example" required>
                                    <option value=""><?php echo $user['role'] ;?></option>
                                    <option value="STUDENT">Student</option>
                                    <option value="CREATOR">Content creator</option>
                                </select>
                                <br>
                                <?php if (!$user['roleapproval']): ?>
                                        <input class="btn btn-outline-light" type="submit" value="Bevestig rol"/>

                                    <?php else: ?>
                                         <input class="btn btn-outline-light" type = "submit" value = "Wijzig rol"/>
                                <?php endif; ?>


                            </form>

                            <br>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col"></div>
    </div>
<?php require '_partials/footer.php'; ?>
</body>

</html>
