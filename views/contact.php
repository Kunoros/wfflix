<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <link rel="stylesheet" href="assets/css/contact.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Required bootstrap from _partials -->


    <title>Contact</title>
</head>

<body>

<div class="container extra-margin">
    <div class="row">
        <div class="col-md-6 text-white">
            <h1>Contact us</h1>
            <p class="extra-margin">We would love to hear from you. Whether you have a question about our services or
                want to give us feedback, our team is ready to answer all your questions and respond to feedback. We will respond to
                your inquiry shortly.</p>
            <div class="row">
                <p>Contact us at our email:</p>
                <a href="mailto:wfflix@hotmail.com" class="email-link">wfflix@hotmail.com</a>
            </div>
            <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11432.036131567813!2d-99.46191965751932!3d44.24805688641287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8780c6b47f0e35af%3A0x975b12a585ed8fce!2sStephan%2C%20South%20Dakota%2057345%2C%20Verenigde%20Staten!5e0!3m2!1snl!2snl!4v1633621709342!5m2!1snl!2snl"
                       class="google-maps" loading="lazy"></iframe>
            </div>
        </div>
        <div class="col-md-6">
            <div class="container py-4 background-form">
                <!-- Bootstrap 5 starter form -->
                <form id="contactForm" method="post" action="./contact-post">
                    <!-- Name input -->
                    <div class="row text-white">
                        <div class="col-md-6 mb-3">
                            <label class="form-label" for="name">NAME *</label>
                            <input class="form-control" name="firstName" type="text" required/>
                            <small id="emailHelp" class="form-text text-white">First</small>
                        </div>
                        <div class="col-md-6 mb-3 lastname-margin">
                            <input class="form-control" name="lastName" type="text" required/>
                            <small id="emailHelp" class="form-text text-white">Last</small>
                        </div>
                    </div>
                    <!-- Email address input -->
                    <div class="row text-white">
                        <div class="mb-3">
                            <label class="form-label" for="emailAddress">EMAIL *</label>
                            <input class="form-control" name="email" type="email" required/>
                        </div>
                    </div>
                    <div class="row text-white">
                        <div class="mb-3">
                            <label class="form-label" for="name">SUBJECT *</label>
                            <input class="form-control" name="subject" type="text" required/>
                        </div>
                    </div>
                    <!-- Message input -->
                    <div class="row text-white">
                        <div class="mb-3">
                            <label class="form-label" for="message">MESSAGE *</label>
                            <textarea class="form-control textarea-height" rows="5" name="message" type="text"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="row text-white">
                        <div class="col-md-6">
                            <p>* indicates required field.</p>
                        </div>
                        <!-- Form submit button -->
                        <div class="col-md-6">
                            <div class="d-grid">
                                <button class="btn btn-outline-light" name="submit" type="submit">Send</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if (!isset($_GET['contact'])) {
                            //       exit();
                        } else {
                            $msgConfirm = $_GET['contact'];

                            if ($msgConfirm == "message has been sent") {
                                echo "<p class='mt-4 msg-confirm'>Message has been sent!</p>";
                            }

                        }

                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require '_partials/footer.php'; ?>
</body>
</html>