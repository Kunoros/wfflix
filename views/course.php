<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/course.css">
    <title>Course</title>
</head>

<body>
    <div class="container-md p-4 main-container">
        <div class="row">
            <div class="video-container">
                <video controls width="100%" class="video">
                    <source src="course/video?id=<?= $course->getCourseId() ?>" type="video/mp4">
                </video>
            </div>

            <div class="info-container p-3 course-info-text">
                <h2 class="h-font mb-2"><?= $course->getCourseName() ?></h2>
                <p class="mb-2 date">Uploaded at: <?= date_format($course->getCreatedAt(), "d/m/Y") ?></p>
                <p class="mb-5 date">Updated at: <?= date_format($course->getUpdatedAt(), "d/m/Y") ?></p>
                <div class="container-fluid">
                    <div class="row mb-4">
                        <div class="col-md-4 mb-3">
                            <h4 class="course-info-text">Course description:</h4>
                        </div>
                        <div class="col-md-5">
                            <p><?= $course->getCourseDescription() ?></p>
                        </div>
                    </div>

                    <hr>

                    <div class="row mb-4">
                        <div class="col-md-4 mb-3">
                            <h4 class="course-info-text">Course creator:</h4>
                        </div>
                        <div class="col-md-6">
                            <p class="course-info-text"><?= $course->getUser()->getFirstName() . ' ' . $course->getUser()->getLastName() ?></p>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <div>
                        <div class="col-md-4 mb-3">
                            <h4 class="course-info-text">Documents:</h4>
                        </div>
                            <ul>
                                <?php foreach ($course->getDocumentList() as $document) : ?>
                                    <li><a class="non-link" href="courses/documents?id=<?= $document->getId() ?>" download><?= $document->getName() ?></a>&nbsp; - <?= date_format($document->getCreatedAt(), "d/m/Y") ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php if ($_SESSION['user']->getId() == $course->getUser()->getId()) : ?>
                            <form action="courses/documents" method="POST" enctype="multipart/form-data">
                                <input type="file" name="file">
                                <button name="courseId" type="submit" value="<?= $course->getCourseId() ?>">Upload</button>
                            </form>
                        <?php endif ?>
                    </div>
                    <hr>

                    <div class="row mb-4">
                        <div class="col-md-4 mb-3">
                            <h4 class="course-info-text">Course rating:</h4>
                        </div>
                        <div class="col-md-6">
                            <p><?= $course->getRating() ?></p>
                        </div>
                    </div>

                    <hr>

                    <div class="row mb-4">
                        <div class="col-md-4 mb-3">
                            <h4 class="course-info-text">Comments:</h4>
                        </div>
                        <div class="col-md-8">
                            <div class="mb-3">
                                <form class="comment-form" action="comment-post" method="POST">
                                    <div class="container comment-form-container">
                                        <div class="row">
                                            <label for="commentBox" class="form-label">Comment</label>
                                            <div class="col-md-12" style="display: flex;">
                                                <textarea type="text" name="comment" class="form-control comment-input" id="commentBox"></textarea>
                                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="mb-3 mt-5">
                                <div class="container">
                                    <?php foreach ($course->getComments() as $comment) : ?>
                                        <div class="row mb-5 comment-box">
                                            <div class="col-sm-4 comment-box1">
                                                <p class="comment-username"><?= $comment->getUser()->getUserName() ?></p>
                                                <p class="comment-upload"><?= date_format($comment->getCreatedAt(), "d/m/Y") ?></p>
                                            </div>
                                            <div class="col-md-8 comment-box2">
                                                <p class="comment-message"><?= $comment->getMessage() ?></p>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php require '_partials/footer.php'; ?>
</body>

</html>