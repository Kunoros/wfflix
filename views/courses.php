<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/courses.css">
    <title>Courses</title>
</head>

<body>
    <div class="container-md main-container" id="course-wrapper">
        <div class="row">
            <?php if($allCourses == false) { ?>
                <div class="container container-no-course">
                    <h1 class="no-course-found">Please try looking for another course.</h1>
                </div>
            <?php }; ?>

            <?php foreach ($allCourses as $course) : ?>
                <div id="col" class="col-sm m-4 p-0 mt-5">
                    <a class="remove-link-decoration" href="course?page=course&id=<?= $course->getCourseId() ?>">
                        <div class="img-container">
                            <img class="img-thumbnail" src="courses/image?id=<?= $course->getCourseId() ?>" alt="course-img" width="100%">
                        </div>
                        <div class="course-card mt-3">
                            <div class="course-title">
                                <h4 class="card-title"><?= $course->getCourseName() ?></h4>
                            </div>
                            <div class="creator-name">
                                <p class="card-name">
                                    <?= $course->getUser()->getFirstName() . ' ' . $course->getUser()->getLastName() ?></p>
                            </div>
                            <div class="course-rating">
                                <p class="card-rating">Rating: <span class="card-rating-number"><?= $course->getRating() ?></span></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php require '_partials/footer.php'; ?>
</body>

</html>