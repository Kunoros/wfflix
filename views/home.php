<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/home.css">

    <title>WFFlix Home</title>
</head>

<body>

    <div class="header">
        <img src="assets/img/wfflix_header.png" class="image-resize-one img-fluid" width="100%">
    </div>

    <div class="container mb-5">
        <!--titel-->
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col"></div>
                    <div class="col-12">
                        <h1><b>What to learn next</b></h1>
                    </div>
                    <div class="col"></div>
                </div>

                <div class="row">
                    <?php foreach ($allCourses as $course) : ?>
                        <div id="col" class="col-sm m-4 p-0 mt-5">
                            <a class="remove-link-decoration" href="course?page=course&id=<?= $course->getCourseId() ?>">
                                <div class="img-container">
                                    <img class="img-thumbnail" src="courses/image?id=<?= $course->getCourseId() ?>" alt="course-img" width="100%">
                                </div>
                                <div class="course-card mt-3">
                                    <div class="course-title">
                                        <h4 class="card-title"><?= $course->getCourseName() ?></h4>
                                    </div>
                                    <div class="creator-name">
                                        <p class="card-name">
                                            <?= $course->getUser()->getFirstName() . ' ' . $course->getUser()->getLastName() ?></p>
                                    </div>
                                    <div class="course-rating">
                                        <p class="card-rating">Rating: <span class="card-rating-number"><?= $course->getRating() ?></span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </div>
    </div>

    <?php require '_partials/footer.php'; ?>
</body>

</html>