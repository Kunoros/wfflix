<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/login.css">
    <title>Login</title>
</head>

<body>
    <div class="text-center mt-5 extra-mb">
        <form class="login-form" action="login-post" method="post">
            <img class="login-img mt-4 mb-4" src="assets/img/customer-register.png" height="72" alt="Customer-img">
            <h1 class="login-h1 h3 mb-3 font-weight-normal">Login</h1>

            <label class="sr-only" for="email">email</label>
            <input type="email" name="email" id="email" class="form-control mb-3" placeholder="Email" required autofocus>

            <label class="sr-only" for="password">password</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>

            <div class="mt-3">
                <button type="submit" name="submit" class="login-btn btn btn-lg btt-block mt-4">Login</button>
            </div>

        </form>
        <?php
        if (!isset($_GET['login'])) {
            // exit();
        } else {
            $loginCheck = $_GET['login'];

            if ($loginCheck == "email not found") {
                echo "<p class='mt-4 login-error'>Email does not exist!</p>";
                //! if exit() is used _partials/footer.php not showing
                // exit();
            } elseif ($loginCheck == "wrong password") {
                echo "<p class='mt-4 login-error'>Wrong password!</p>";
            } 
            
        }
        ?>
    </div>

    <?php require '_partials/footer.php'; ?>
</body>

</html>