<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/profile.css">
    <title>Profile</title>
</head>

<body>
    <div class="container main-container">
        <div class="row">
            <div class="col-md-4 mt-1">
                <div class="card text-center sidebar">
                    <div class="card-body">
                        <div class="mt-3">
                            <h2><?= $_SESSION['user']->getUsername(); ?></h2>
                            <hr>
                            <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#edit" aria-expanded="false" aria-controls="edit">
                                Change details
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 mt-1 ">

                <div class="card md-3 content collapse" id="edit">
                    <h1 class="m-3 pt-3">Edit profile</h1>
                    <div class="card-body">
                        <form method="post" action="./update-profile">
                            <input type="hidden" value="<?= $_SESSION['user']->getId(); ?>" name="id">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="firstName">First name</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="firstName" type="text" name="firstName" class="form-control" value="<?= $_SESSION['user']->getFirstName(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="lastName">Last name</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="lastName type=" text" name="lastName" class="form-control" value="<?= $_SESSION['user']->getLastName(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="userName">Username</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="userName" type="text" name="userName" class="form-control" value="<?= $_SESSION['user']->getUserName(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="email" type="email" name="email" class="form-control" value="<?= $_SESSION['user']->getEmail(); ?>" required>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label for="newPassword">New password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="newPassword" type="password" name="new_password" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <label for="confirmPassword">Confirm password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="confirmPassword" type="password" name="new_password2" class="form-control">
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <label for="oldPassword">Old password</label>
                                </div>
                                <div class="col-md-9 text-secondary">
                                    <input id="oldPassword" type="password" name="old_password" class="form-control">
                                </div>
                            </div>
                    </div>
                    <hr>
                    <div class="m-auto mb-3 mt-2 justify-content-center">
                        <button class="btn" type="submit" name="submit">Change!</button>
                    </div>
                    <form>
                </div>

                <div class="card md-3 content" id="account">
                    <h1 class="m-3 pt-3">About</h1>
                    <?php if (!isset($_GET['edit'])) {
                    } else {
                        $loginCheck = $_GET['edit'];

                        if ($loginCheck == "Something went wrong try again!") {
                            echo "<p class='m-3 login-error'>Something went wrong try again!</p>";
                        } elseif ($loginCheck == "User not found!") {
                            echo "<p class='m-3 login-error'>User not found!</p>";
                        }
                        elseif ($loginCheck == "Verify went wrong!") {
                            echo "<p class='m-3 login-error'>Verify went wrong!</p>";
                        }
                    } ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Full name</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                <span><?= $_SESSION['user']->getFirstName(); ?> </span> <span><?= $_SESSION['user']->getLastName(); ?> </span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Email</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                <?= $_SESSION['user']->getEmail(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="card mb-3 content">
                    <h1 class="m-3">Followed courses</h1>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Course name</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                Course link
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Course name</h5>
                            </div>
                            <div class="col-md-9 text-secondary">
                                Course link
                            </div>
                        </div>
                    </div>
                </div> -->
                <?php
                if ($_SESSION['user']->getRole() == 'CREATOR'){
                ?>
                <div class="card mb-3 content">
                    <h1 class="m-3">My courses</h1>
                    <?php foreach ($Mycourse as $courses) :?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Course name</h4>
                                </div>
                                <div class="col-md-4 text-secondary">
                                    <h4> <?php echo $courses->getCourseName() ?></h4>
                                </div>
                                <div class="col-md-2">
                                    <a style="color: black;"  href="profile?type=delete&id=<?=$courses->getCourseId()?>"><i class="col-md-3 bi bi-trash"></i></a>
                                </div>
                            </div>
                            <hr>
                        </div>

                    <?php
                    endforeach;
                    if (empty($Mycourse)){
                        echo "<h4 class='m-3'>U heeft nog geen courses geupload</h4>";
                    }
                    }?>
                </div>
            </div>
        </div>
    </div>
    <?php require '_partials/footer.php'; ?>
</body>

</html>