<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/register.css">
    <title>Register</title>
</head>

<body>
<div class="text-center mt-5 extra-mb">
    <div class="container pt-4 pb-5">
        <form action="./register-post" method="post">
        <img class="login-img mt-4 mb-4" src="assets/img/customer-register.png" height="72" alt="Customer-img">
            <div class="row px-md-4 px-1 m-0">
                <div class="col-12">
                </div>
                <div class="col-6"> <input type="text" class="form-control mb-4" name="firstName" placeholder="First name" required> </div>
                <div class="col-6"> <input type="text" class="form-control mb-4" name="lastName" placeholder="Last name" required> </div>
                <div class="col-6"> <input type="text" class="form-control mb-4" name="userName" placeholder="User name" required> </div>
                <div class="col-6"> <input type="email" class="form-control mb-4" name="email" placeholder="Email" required> </div>
                <div class="col-12"> <input type="password" class="form-control mb-4" name="password" placeholder="Password" required> </div>
                <br>
                <div class="radioLeft">
                <input type="radio" name="role"
                    <?php if (isset($role) && $role=="Student") echo "checked";?>
                       value="student">Student
                <br>

                <input type="radio" name="role"
                    <?php if (isset($role) && $role=="Creator") echo "checked";?>
                       value="contentcreator">Content creator
                </div>
                <br>
                <div class="col-12 create">
                    <br>
                    <div class="d-flex align-items-center justify-content-between">
                        <button type="submit" name="submit" class="btn btn-primary">Create Your Account</button>
                    </div>
                </div>
            </div>
        </form>
        <?php
        if (!isset($_GET['register'])) {
            // exit();
        } else {
            $registerCheck = $_GET['register'];

            if ($registerCheck == "email already in use") {
                echo "<p class='mt-4 login-error'>The email is already in use!</p>";
                //! if exit() is used _partials/footer.php not showing
                // exit();
            }
        }
        ?>
    </div>
    </div>
    <?php require '_partials/footer.php'; ?>
</body>

</html>