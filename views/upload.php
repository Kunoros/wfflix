<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/upload.css">
    <title>Upload</title>
</head>

<body>
<div class="container py-4 background-form extra-margin main-container">
    <!-- Bootstrap 5 starter form -->
    <form class="form-container" method="post" action="/upload-post" enctype="multipart/form-data">
        <div class="container ">
            <div class="row">
                <div class="mb-3 form-group text-white">
                    <label>Course name</label>
                    <input type="text" maxlength="30" name="courseName" class="form-control"
                           placeholder="Course name" required>
                </div>
                <div class="mb-3 text-white">
                    <label for="formFile">Thumbnail</label>
                    <input name="file" accept="image/png, image/jpeg" maxlength="100" class="form-control" type="file"
                           id="formFile">
                </div>
                <div class="mb-3 form-group text-white">
                    <label>Description</label>
                    <input type="text" maxlength="30" name="courseDescription" class="form-control"
                           placeholder="Description" required>
                </div>
                <div class="mb-3 text-white">
                    <label for="formFile">Video file</label>
                    <input name="video" maxlength="100" accept="video/*" class="form-control" type="file" id="formFile">
                </div>


                <div class="text-center">
                    <button type="submit" name="submit" class="btn btn-primary" style="width: 50%;">Submit</button>
                </div>
            </div>
        </div>
    </form>
    <?php
    if (!isset($_GET['upload'])) {
        // exit();
    } else {
        $loginCheck = $_GET['upload'];

        if ($loginCheck == "succes inserted into data base") {
            echo "<p class='mt-4 upload-succes'>Success, inserted into database</p>";
            //! if exit() is used _partials/footer.php not showing
            // exit();
        } elseif ($loginCheck == "now rights") {
            echo "<p class='mt-4 login-error'>U heeft geen rechten om deze actie uit te voeren</p>";
        }
    } ?>

</div>